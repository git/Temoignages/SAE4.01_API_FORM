<?php

namespace ExceptionHandle;

use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpSpecializedException;
use Throwable;

class TypeErrorParameters extends HttpSpecializedException {

    protected $code = 400;
    protected string $title = "Method query params is not specified";
    protected $message =  "Bad Parameters, The API need parameters for this method. Exemple :'http://url/method?param1=1'";

    public function __construct(ServerRequestInterface $request,?Throwable $previous = null)
    {
        parent::__construct($request, $this->message, $previous);
    }

}
