<?php

namespace ExceptionHandle;

use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpSpecializedException;
use Throwable;

class HttpNotFoundError extends HttpSpecializedException {

    protected $code = 404;
    protected string $title = "Method Not Found";
    protected $message =  "You have to add a method name in the URL. Example !: 'http://url/method'";

    public function __construct(ServerRequestInterface $request,?Throwable $previous = null)
    {
        parent::__construct($request, $this->message, $previous);
    }

}