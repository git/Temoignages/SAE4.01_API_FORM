<?php

namespace ExceptionHandle;

use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpSpecializedException;
use Throwable;

class PDOError extends HttpSpecializedException {

    protected $code = 408;
    protected string $title = "PDO connection failed";
    protected string $file;
    protected int $line;

    public function __construct(ServerRequestInterface $request,string $message,?Throwable $previous = null)
    {
        $this->file = $previous->getFile();
        $this->line = $previous->getLine();
        parent::__construct($request,$message,$previous);
    }

}