<?php

use ExceptionHandle\HttpNotFoundError;
use ExceptionHandle\PDOError;
use ExceptionHandle\TypeErrorParameters;
use Gateway\GatewayForm;
use Gateway\GatewayKeyword;
use Gateway\GatewayListResponseOfCandidate;
use Gateway\GatewayPossibleResponse;
use Gateway\GatewayQuestion;
use Gateway\GatewayAdmin;
use Gateway\GatewayResponse;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;

require './Config/vendor/autoload.php';

/**
 * Instantiate App
 */
$app = AppFactory::create();

// Add Routing Middleware
$app->addRoutingMiddleware();

/**
 * Add Error Handling Middleware
 *
 * @param bool $displayErrorDetails -> Should be set to false in production
 * @param bool $logErrors -> Parameter is passed to the default ErrorHandler
 * @param bool $logErrorDetails -> Display error details in error log
 */
$errorMiddleware = $app->addErrorMiddleware(true, true, true);

/**
 * Add a route for the API
 */
$app->get('/', function (Request $request) {
    throw new HttpNotFoundError($request);
});

$app->get('/getForm', function(Request $request, Response $response){
    try{
        $response->getBody()->write(json_encode((new GatewayForm)->getForm(), JSON_UNESCAPED_UNICODE));
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    return $response->withHeader('Content-type', 'application/json')->withStatus(200);
});

$app->post('/insertForm', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['title']) || empty($parameters['description'])){
        throw new TypeErrorParameters($request);
    }
    try{
        (new GatewayForm)->insertForm($parameters['title'],$parameters['description']);
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    $response->getBody()->write("OK");
    return $response->withStatus(200);
});

$app->delete('/deleteForm', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['id'])){
        throw new TypeErrorParameters($request);
    }
    try{
        (new GatewayForm)->deleteForm($parameters['id']);
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    $response->getBody()->write("OK");
    return $response->withStatus(200);
});

$app->get('/existsForm', function(Request $request, Response $response){
    try{
        $response->getBody()->write(json_encode((new GatewayForm)->existsForm(), JSON_UNESCAPED_UNICODE));
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    return $response->withHeader('Content-type', 'application/json')->withStatus(200);
});

$app->post('/assignKeywordToQuestion', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['keyword']) || empty($parameters['id']) || empty($parameters['response'])){
        throw new TypeErrorParameters($request);
    }
    try{
        (new GatewayForm)->assignKeywordToQuestion($parameters['keyword'],$parameters['id'],$parameters['response']);
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    $response->getBody()->write("OK");
    return $response->withStatus(200);
});

$app->delete('/deleteKeywordFromQuestion', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['keyword']) || empty($parameters['id']) || empty($parameters['response'])){
        throw new TypeErrorParameters($request);
    }
    try{
        (new GatewayForm)->deleteKeywordFromQuestion($parameters['keyword'],$parameters['id'],$parameters['response']);
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    $response->getBody()->write("OK");
    return $response->withStatus(200);
});

$app->put('/updateTitleToForm', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['id']) || empty($parameters['title'])){
        throw new TypeErrorParameters($request);
    }
    try{
        (new GatewayForm)->updateTitleToForm($parameters['id'],$parameters['title']);
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    $response->getBody()->write("OK");
    return $response->withStatus(200);
});

$app->put('/updateDescriptionToForm', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['id']) || empty($parameters['description'])){
        throw new TypeErrorParameters($request);
    }
    try{
        (new GatewayForm)->updateDescriptionToForm($parameters['id'],$parameters['description']);
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    $response->getBody()->write("OK");
    return $response->withStatus(200);
});

$app->delete('/deleteDescriptionToForm', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['id'])){
        throw new TypeErrorParameters($request);
    }
    try{
        (new GatewayForm)->deleteDescriptionToForm($parameters['id']);
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    $response->getBody()->write("OK");
    return $response->withStatus(200);
});

$app->post('/insertKeyword', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['keyword'])){
        throw new TypeErrorParameters($request);
    }
    try{
        (new GatewayKeyword)->insertKeyword($parameters['keyword']);
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    $response->getBody()->write("OK");
    return $response->withStatus(200);
});

$app->delete('/deleteKeyword', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['keyword'])){
        throw new TypeErrorParameters($request);
    }
    try{
        (new GatewayKeyword)->deleteKeyword(($parameters['keyword']));
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    $response->getBody()->write("OK");
    return $response->withStatus(200);
});

$app->get('/getAllKeyword', function(Request $request, Response $response){
    try{
        $response->getBody()->write(json_encode((new GatewayKeyword)->getAllKeyword(),JSON_UNESCAPED_UNICODE));
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    return $response->withHeader('Content-type', 'application/json')->withStatus(200);

});

$app->get('/getKeywordsContentByReference', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['id'])){
        throw new TypeErrorParameters($request);
    }
    try{
        $response->getBody()->write(json_encode((new GatewayKeyword)->getKeywordsContentByReference($parameters['id']),JSON_UNESCAPED_UNICODE));
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    return $response->withHeader('Content-type', 'application/json')->withStatus(200);
});

$app->get('/getDetailsListResponseOfCandidate', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['id'])){
        throw new TypeErrorParameters($request);
    }
    try{
        $response->getBody()->write(json_encode((new GatewayListResponseOfCandidate)->getDetailsListResponsesOfCandidate($parameters['id']),JSON_UNESCAPED_UNICODE));
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    return $response->withHeader('Content-type', 'application/json')->withStatus(200);
});

$app->get('/getAllListResponseOfCandidate', function(Request $request, Response $response){
    try{
        $response->getBody()->write(json_encode((new GatewayListResponseOfCandidate)->getAllListResponsesOfCandidate(),JSON_UNESCAPED_UNICODE));
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    return $response->withHeader('Content-type', 'application/json')->withStatus(200);
});

$app->delete('/deleteListResponseOfCandidate', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['id'])){
        throw new TypeErrorParameters($request);
    }
    try{
        (new GatewayListResponseOfCandidate)->deleteListResponseOfCandidate($parameters['id']);
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    $response->getBody()->write("OK");
    return $response->withStatus(200);
});

$app->post('/insertListResponseOfCandidate', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['id']) || empty($parameters['answer']) || empty($parameters['category']) || empty($parameters['titleForm'])){
        throw new TypeErrorParameters($request);
    }
    try{
        (new GatewayListResponseOfCandidate)->insertListResponsesOfCandidate(array_map('intval', explode(",",$parameters['id'])),explode(",",$parameters['answer']),explode(",",$parameters['category']), $parameters['titleForm']);
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    $response->getBody()->write("OK");
    return $response->withStatus(200);
});

$app->get('/getPossibleResponseByQuestion', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['id'])){
        throw new TypeErrorParameters($request);
    }
    try{
        $response->getBody()->write(json_encode((new GatewayPossibleResponse)->getPossibleResponseByQuestion($parameters['id']),JSON_UNESCAPED_UNICODE));
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    return $response->withHeader('Content-type', 'application/json')->withStatus(200);
});

$app->post('/insertPossibleResponse', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['content'])){
        throw new TypeErrorParameters($request);
    }
    try{
        $response->getBody()->write(json_encode((new GatewayPossibleResponse)->insertPossibleResponse($parameters['content']),JSON_UNESCAPED_UNICODE));
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    return $response->withHeader('Content-type', 'application/json')->withStatus(200);
});

$app->get('/getResponsesByIdListCandidate', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['id'])){
        throw new TypeErrorParameters($request);
    }
    try{
        $response->getBody()->write(json_encode((new GatewayResponse)->getResponsesByIdListCandidate($parameters['id']),JSON_UNESCAPED_UNICODE));
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    return $response->withHeader('Content-type', 'application/json')->withStatus(200);
});

$app->get('/getResponsesIdByIdListCandidate', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['id'])){
        throw new TypeErrorParameters($request);
    }
    try{
        $response->getBody()->write(json_encode((new GatewayResponse)->getResponsesIdByIdListCandidate($parameters['id']),JSON_UNESCAPED_UNICODE));
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    return $response->withHeader('Content-type', 'application/json')->withStatus(200);
});

$app->delete('/deleteResponseById', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['id'])){
        throw new TypeErrorParameters($request);
    }
    try{
        (new GatewayResponse)->deleteResponseById($parameters['id']);
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    $response->getBody()->write("OK");
    return $response->withStatus(200);
});

$app->post('/insertResponse', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['content']) || empty($parameters['questionContent']) || empty($parameters['category'])){
        throw new TypeErrorParameters($request);
    }
    try{
        $response->getBody()->write(json_encode((new GatewayResponse)->insertResponse($parameters['content'],$parameters['questionContent'],$parameters['category']),JSON_UNESCAPED_UNICODE));
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    return $response->withHeader('Content-type', 'application/json')->withStatus(200);
});

$app->post('/addQuestion', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['content']) || empty($parameters['classQuestion']) || empty($parameters['idForm'])){
        throw new TypeErrorParameters($request);
    }
    try{
       $response->getBody()->write(json_encode((new GatewayQuestion)->addQuestion($parameters['content'],$parameters['classQuestion'],$parameters['idForm']),JSON_UNESCAPED_UNICODE));
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    return $response->withHeader('Content-type', 'application/json')->withStatus(200);
});

$app->delete('/deleteQuestion', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['classQuestion']) || empty($parameters['id'])){
        throw new TypeErrorParameters($request);
    }
    try{
        (new GatewayQuestion)->deleteQuestion($parameters['classQuestion'],$parameters['id']);
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    $response->getBody()->write("OK");
    return $response->withStatus(200);
});

$app->put('/updateQuestion', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['content']) || empty($parameters['classQuestion']) || empty($parameters['questionGetForm']) || empty($parameters['id'])){
        throw new TypeErrorParameters($request);
    }
    try{
        (new GatewayQuestion)->updateQuestion($parameters['content'],$parameters['classQuestion'],$parameters['questionGetForm'], $parameters['id']);
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    $response->getBody()->write("OK");
    return $response->withStatus(200);
});

$app->get('/getAllQuestions', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['idForm'])){
        throw new TypeErrorParameters($request);
    }
    try{
        $response->getBody()->write(json_encode((new GatewayQuestion)->getAllQuestions($parameters['idForm']),JSON_UNESCAPED_UNICODE));
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    return $response->withHeader('Content-type', 'application/json')->withStatus(200);
});

$app->get('/getQuestionContentById', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['id'])){
        throw new TypeErrorParameters($request);
    }
    try{
        $response->getBody()->write(json_encode((new GatewayQuestion)->getQuestionContentById($parameters['id']),JSON_UNESCAPED_UNICODE));
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    return $response->withHeader('Content-type', 'application/json')->withStatus(200);
});

$app->get('/getKeywordContentByCategorize', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['id'])){
        throw new TypeErrorParameters($request);
    }
    try{
        $response->getBody()->write(json_encode((new GatewayKeyword)->getKeywordsContentByCategorize($parameters['id']),JSON_UNESCAPED_UNICODE));
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    return $response->withHeader('Content-type', 'application/json')->withStatus(200);
});

$app->get('/getPasswordWithLogin', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['login'])){
        throw new TypeErrorParameters($request);
    }
    try{
        $response->getBody()->write(json_encode((new GatewayAdmin())->getPasswordWithLogin($parameters['login']),JSON_UNESCAPED_UNICODE));
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    return $response->withHeader('Content-type', 'application/json')->withStatus(200);
});

$app->delete('/deletePossibleResponse', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['id'])){
        throw new TypeErrorParameters($request);
    }
    try{
        (new GatewayPossibleResponse)->deletePossibleResponse($parameters['id']);
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    $response->getBody()->write("OK");
    return $response->withStatus(200);
});

$app->post('/insertResponseInQuestion', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['response']) || empty($parameters['categories']) || empty($parameters['idQuestion'])){
        throw new TypeErrorParameters($request);
    }
    try{
        (new GatewayQuestion)->insertResponseInQuestion($parameters['response'],json_decode($parameters['categories']),$parameters['idQuestion']);
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    $response->getBody()->write("OK");
    return $response->withStatus(200);
});

$app->get('/getCategorizeOfResponsesIdByIdListCandidate', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['listResponse'])){
        throw new TypeErrorParameters($request);
    }
    try{
        $response->getBody()->write(json_encode((new GatewayResponse)->getCategorizeOfResponsesIdByIdListCandidate($parameters['listResponse']),JSON_UNESCAPED_UNICODE));
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    return $response->withHeader('Content-type', 'application/json')->withStatus(200);
});

$app->get('/getResponsesByQueryAndIdListCandidate', function(Request $request, Response $response){
    $parameters = $request->getQueryParams();
    if (empty($parameters['query']) || empty($parameters['id'])){
        throw new TypeErrorParameters($request);
    }
    try{
        $response->getBody()->write(json_encode((new GatewayResponse)->getResponsesByQueryAndIdListCandidate($parameters['query'],$parameters['id']),JSON_UNESCAPED_UNICODE));
    }catch (PDOException $e){
        throw new PDOError($request,$e->getMessage(),$e);
    }
    return $response->withHeader('Content-type', 'application/json')->withStatus(200);
});


// Run app
$app->run();
