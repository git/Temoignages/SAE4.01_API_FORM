<?php

namespace Config;

use PDOException;

class ScriptDatabase {

    private Connection $connection;
    public function __construct() {
        try{
            $this->connection = (new ConnectClass)->connect();
        }catch(PDOException $e){
            throw new PDOException($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function executeScript(): void
    {
        $queryScript = '

CREATE TABLE `admin` (
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `categorize`
--

CREATE TABLE `categorize` (
  `response` int(11) NOT NULL,
  `keyword` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `form`
--

CREATE TABLE `form` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `form`
--

-- --------------------------------------------------------

--
-- Structure de la table `keyword`
--

CREATE TABLE `keyword` (
  `word` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `listresponsesofcandidate`
--

CREATE TABLE `listresponsesofcandidate` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `titleForm` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `possibleresponse`
--

CREATE TABLE `possibleresponse` (
  `id` int(11) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `propose`
--

CREATE TABLE `propose` (
  `question` int(11) NOT NULL,
  `possibleResponse` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `question`
--

CREATE TABLE `question` (
  `id` int(11) NOT NULL,
  `content` text NOT NULL,
  `type` varchar(50) NOT NULL,
  `form` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `reference`
--

CREATE TABLE `reference` (
  `keyword` varchar(50) NOT NULL,
  `possibleResponse` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `response`
--

CREATE TABLE `response` (
  `id` int(11) NOT NULL,
  `content` varchar(200) NOT NULL,
  `questionContent` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `submit`
--

CREATE TABLE `submit` (
  `responsesCandidate` int(11) NOT NULL,
  `response` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`username`);

--
-- Index pour la table `categorize`
--
ALTER TABLE `categorize`
  ADD PRIMARY KEY (`response`,`keyword`),
  ADD KEY `keyword` (`keyword`);

--
-- Index pour la table `form`
--
ALTER TABLE `form`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `keyword`
--
ALTER TABLE `keyword`
  ADD PRIMARY KEY (`word`);

--
-- Index pour la table `listresponsesofcandidate`
--
ALTER TABLE `listresponsesofcandidate`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `possibleresponse`
--
ALTER TABLE `possibleresponse`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `propose`
--
ALTER TABLE `propose`
  ADD PRIMARY KEY (`question`,`possibleResponse`),
  ADD KEY `possibleResponse` (`possibleResponse`);

--
-- Index pour la table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`id`),
  ADD KEY `form` (`form`);

--
-- Index pour la table `reference`
--
ALTER TABLE `reference`
  ADD PRIMARY KEY (`keyword`,`possibleResponse`),
  ADD KEY `possibleResponse` (`possibleResponse`);

--
-- Index pour la table `response`
--
ALTER TABLE `response`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `submit`
--
ALTER TABLE `submit`
  ADD PRIMARY KEY (`responsesCandidate`,`response`),
  ADD KEY `response` (`response`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `form`
--
ALTER TABLE `form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `listresponsesofcandidate`
--
ALTER TABLE `listresponsesofcandidate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `possibleresponse`
--
ALTER TABLE `possibleresponse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT pour la table `question`
--
ALTER TABLE `question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT pour la table `response`
--
ALTER TABLE `response`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `categorize`
--
ALTER TABLE `categorize`
  ADD CONSTRAINT `Categorize_ibfk_2` FOREIGN KEY (`response`) REFERENCES `response` (`id`),
  ADD CONSTRAINT `categorize_ibfk_1` FOREIGN KEY (`keyword`) REFERENCES `keyword` (`word`);

--
-- Contraintes pour la table `propose`
--
ALTER TABLE `propose`
  ADD CONSTRAINT `Propose_ibfk_1` FOREIGN KEY (`possibleResponse`) REFERENCES `possibleresponse` (`id`),
  ADD CONSTRAINT `Propose_ibfk_2` FOREIGN KEY (`question`) REFERENCES `question` (`id`);

--
-- Contraintes pour la table `question`
--
ALTER TABLE `question`
  ADD CONSTRAINT `Question_ibfk_1` FOREIGN KEY (`form`) REFERENCES `form` (`id`);

--
-- Contraintes pour la table `reference`
--
ALTER TABLE `reference`
  ADD CONSTRAINT `reference_ibfk_1` FOREIGN KEY (`possibleResponse`) REFERENCES `possibleresponse` (`id`);

--
-- Contraintes pour la table `submit`
--
ALTER TABLE `submit`
  ADD CONSTRAINT `Submit_ibfk_1` FOREIGN KEY (`response`) REFERENCES `response` (`id`),
  ADD CONSTRAINT `Submit_ibfk_2` FOREIGN KEY (`responsesCandidate`) REFERENCES `listresponsesofcandidate` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
';
        $this->connection->executeQuery($queryScript);
        $queryScript = "INSERT INTO `form` (`id`, `title`, `description`) VALUES (1, 'Votre avis nous intéresse !', 'Ce formulaire vous permet de candidater à une potentielle interview si votre profil nous intéresse.')";
        $this->connection->executeQuery($queryScript);
    }
}
