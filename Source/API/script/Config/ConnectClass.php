<?php

namespace Config;

use ExceptionHandle\PDOError;
use PDOException;

require_once __DIR__ ."/Connection.php";

class ConnectClass{

    private string $dsn;
    private string $login;
    private string $password;

    function __construct(){
        if ($_ENV["HOST"] == null || $_ENV["DATABASE"] == null || $_ENV["USER"] == null || $_ENV["PASSWORD"] == null){
            throw new PDOException("ENV variable not found");
        }
        $this->dsn = "mysql:host=".$_ENV["HOST"].";dbname=".$_ENV["DATABASE"].";charset=UTF8";
        $this->login = $_ENV["USER"];
        $this->password = $_ENV["PASSWORD"];
    }

    function connect(): int|Connection
    {
        try {
            echo "  ";
            $connection = new Connection($this->dsn,$this->login,$this->password);
        }catch (PDOException $e){
            throw new PDOException($e->getMessage(), $e->getCode(), $e);
        }
        return $connection;
    }


}
