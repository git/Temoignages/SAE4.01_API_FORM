<?php

namespace Gateway;

use Config\Connection;
use Config\ConnectClass;
use PDO;
use PDOException;

class GatewayPossibleResponse
{
    /**
     * @var Connection
     */
    private Connection $connection;

    public function __construct()
    {
        try{
            $this->connection = (new ConnectClass)->connect();
        }catch(PDOException $e){
            throw new PDOException($e->getMessage(), $e->getCode(), $e);
        }
    }
    /**
     * Permet de récupérer les différentes réponses possibles à une question donnée.
     *
     * @param int $idQuestion Id de la question pour laquelle on veut récupérer les réponses possibles
     *
     * @return array Retourne la liste de possibles réponses
     */
    public function getPossibleResponseByQuestion(int $idQuestion): array
    {
        $query = "SELECT pr.* FROM `propose` p, `possibleresponse` pr 
                        WHERE p.question = :questionId AND p.possibleResponse = pr.id";
        $this->connection->executeQuery($query, array(
            ':questionId' => array($idQuestion, PDO::PARAM_INT)
        ));

        return $this->connection->getResults();
    }

    /**
     * Permet d'insérer une possible réponse dans la base de donnée.
     *
     * @param string $contentPossibleResponse Contenu de la possible réponse
     *
     * @return int
     */
    public function insertPossibleResponse(string $contentPossibleResponse): int
    {
        $query = "INSERT INTO `possibleresponse`(content) VALUES(:content)";
        $this->connection->executeQuery($query, array(
            ':content' => array($contentPossibleResponse, PDO::PARAM_STR)
        ));

        return $this->connection->lastInsertId();
    }

    /**
     * Permet de supprimer une possible réponse de la base de donnée par son id.
     *
     * @param int $id Id de la possible réponse à supprimer
     *
     * @return void
     */
    public function deletePossibleResponse(int $id): void
    {
        $query = "DELETE FROM `reference` WHERE response = :id";
        $this->connection->executeQuery($query, array(
            ':id' => array($id, PDO::PARAM_INT)
        ));

        $query = "DELETE FROM `possibleresponse` WHERE id = :id";
        $this->connection->executeQuery($query, array(
            ':id' => array($id, PDO::PARAM_INT)
        ));
    }
}