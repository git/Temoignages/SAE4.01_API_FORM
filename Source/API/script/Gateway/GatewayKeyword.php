<?php

namespace Gateway;

use Config\Connection;
use Config\ConnectClass;
use PDO;
use PDOException;

class GatewayKeyword
{
    /**
     * @var Connection
     */
    private Connection $connection;

    public function __construct()
    {
        try{
            $this->connection = (new ConnectClass)->connect();
        }catch(PDOException $e){
            throw new PDOException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Permet d'ajouter un Keyword dans la base de donnée.
     *
     * @param string $keyword Keyword à ajouter
     *
     * @return void
     */
    public function insertKeyword(string $keyword): void
    {
        $query = "INSERT INTO `keyword`(word) VALUES(:word)";
        $this->connection->executeQuery($query, array(
            ':word' => array($keyword, PDO::PARAM_STR)
        ));
    }

    /**
     * Permet de supprimer un keyword de la base de donnée.
     *
     * @param string $keyword Keyword à supprimer
     *
     * @return void
     */
    public function deleteKeyword(string $keyword): void
    {
        $query = "DELETE FROM `keyword` WHERE word = :word";
        $this->connection->executeQuery($query, array(
            ':word' => array($keyword, PDO::PARAM_STR)
        ));
    }

    /**
     * Permet de récupérer l'ensemble des Keyword disponible.
     *
     * @return array Retourne la liste de l'ensemble des keyword sauvé dans la base de donnée
     */
    public function getAllKeyword(): array
    {
        $query = "SELECT * FROM `keyword`";
        $this->connection->executeQuery($query);
        return $this->connection->getResults();
    }

    /**
     * Permet de récupérer tous les Keyword qui font référence à l'id de la réponse possible à une question passée en paramètre.
     *
     * @param int $id Id de la possible réponse que l'on veut connaitre ses catégories
     *
     * @return array Retourne l'ensemble de tous les Keyword associer à la réponse
     */
    public function getKeywordsContentByReference(int $id): array
    {
        $query = "SELECT k.* FROM `keyword` k, `reference` r 
                        WHERE k.word = r.keyword AND r.possibleResponse = :id";
        $this->connection->executeQuery($query, array(
            ':id' => array($id, PDO::PARAM_STR)
        ));


        $tab = [];
        foreach ($this->connection->getResults() as $result)
        {
               $tab[] = $result["word"];
        }
        return $tab;
    }

    /**
     * Permet de récupérer tous les Keyword qui font référence à l'id de la réponse d'un candidat passée en paramètre.
     *
     * @param int $id Id de la réponse que l'on veut connaitre ses catégories
     *
     * @return array Retourne l'ensemble de tous les Keyword associer à la réponse
     */
    public function getKeywordsContentByCategorize(int $id): array
    {
        $query = "SELECT k.* FROM `keyword` k, `categorize` c
                        WHERE k.word = c.keyword AND c.response = :id";

        return $this->getKeywordByAssociation($id , $query);
    }

    /**
     * Permet de récupérer une liste de Keyword selon une requête donnée et un id associé.
     *
     * @param int $id Id de l'objet dont on veut ses Keyword associé
     * @param string $query Requête que l'on veut exécuter
     *
     * @return array Retourne la liste des différents Keyword associé à l'objet voulu
     */
    private function getKeywordByAssociation(int $id, string $query): array
    {
        $this->connection->executeQuery($query, array(
            ':id' => array($id, PDO::PARAM_STR)
        ));

        $tab = [];
        foreach ($this->connection->getResults() as $result) {
            $tab[] = $result["word"];
        }

        return $tab;
    }
}