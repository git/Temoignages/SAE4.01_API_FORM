<?php

namespace Gateway;

use Config\Connection;
use Config\ConnectClass;
use PDO;
use PDOException;
use Slim\Psr7\Stream;

/**
 * Permet d'accéder, d'écrire ou de modifier les données contenues dans la table Admin afin de gérer l'espace administrateur.
 */
class GatewayAdmin
{
    /**
     * @var Connection
     */
    private Connection $connection;

    public function __construct()
    {
        try{
            $this->connection = (new ConnectClass)->connect();
        }catch(PDOException $e){
            throw new PDOException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Permet de récupérer le mot de passe de l'administrateur en fonction de son login.
     * @param String $login Le login de l'administrateur.
     * @return String|null Le mot de passe de l'administrateur ou null si l'administrateur n'existe pas.
     */

    public function getPasswordWithLogin(String $login): ?string
    {
        $query = "SELECT password FROM `admin` WHERE username = :username";
        $this->connection->executeQuery($query, array(
            ':username' => array($login, PDO::PARAM_STR)
        ));
        $result = $this->connection->getResults();
        if(empty($result))
            return null;
        return $result[0]['password'];
    }

    public function addAdmin(String $login, String $password): void
    {
        $query = "INSERT INTO `admin`(username, password) VALUES(:username, :password)";
        $this->connection->executeQuery($query, array(
            ':username' => array($login, PDO::PARAM_STR),
            ':password' => array($password, PDO::PARAM_STR)
        ));
    }
}