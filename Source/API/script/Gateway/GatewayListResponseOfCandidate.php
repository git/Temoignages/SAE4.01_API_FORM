<?php

namespace Gateway;

use Config\Connection;
use Config\ConnectClass;
use PDO;
use PDOException;

class GatewayListResponseOfCandidate
{
    /**
     * @var Connection
     */
    private Connection $connection;


    public function __construct()
    {
        try{
            $this->connection = (new ConnectClass)->connect();
        }catch(PDOException $e){
            throw new PDOException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Permet de récupérer la liste détaillée des réponses avec leurs catégories associer
     * aux différentes questions qu'un candidat cible a répondues
     *
     * @param int $idListResponse Id du candidat pour lequel on veut récupérer ses réponses
     *
     * @return array Retourne une liste qui pour
     * l'indice 0 a la liste des infos du candidat
     * l'indice 1 la liste des réponses
     * l'indice 2 une liste d'autres listes de catégories, qui pour chaque même indice correspond à la liste des catégories de la réponse
     */
    public function getDetailsListResponsesOfCandidate(int $idListResponse): array
    {
        $gatewayResponse = new GatewayResponse();
        $gatewayKeyword = new GatewayKeyword();
        $tabKeywords = [];

        $query = "SELECT * FROM `listresponsesofcandidate` WHERE id = :id";
        $this->connection->executeQuery($query, array(
            ':id' => array($idListResponse, PDO::PARAM_INT)
        ));

        $questionList = $this->connection->getResults()[0];

        $responses = $gatewayResponse->getResponsesByIdListCandidate($questionList['id']);

        foreach ($responses as $row) {
            $tabKeywords[] = $gatewayKeyword->getKeywordsContentByReference($row['id']);
        }

        return array($questionList, $responses, $tabKeywords);
    }

    /**
     * Permet de récupérer la liste des personnes ayant répondu au formulaire
     *
     * @return array Retourne la liste brute des informations de tous les candidats ayant répondu au formulaire
     */
    public function getAllListResponsesOfCandidate(): array
    {
        $query = "SELECT * FROM `listresponsesofcandidate`";
        $this->connection->executeQuery($query);

        return $this->connection->getResults();
    }

    /**
     * Permet de supprimer la liste des réponses d'un candidat de la base de donnée
     *
     * @param int $id Id du candidat à supprimer
     *
     * @return void
     */
    public function deleteListResponseOfCandidate(int $id): void
    {
        $gatewayResponse = new GatewayResponse();

        foreach ( $gatewayResponse->getResponsesIdByIdListCandidate($id) as $response){
            $gatewayResponse->deleteResponseById($response);
        }

        $query = "DELETE FROM `listresponsesofcandidate` WHERE id = :id";
        $this->connection->executeQuery($query, array(
            'id' => array($id, PDO::PARAM_STR)
        ));
    }

    /**
     * Permet d'insérer dans la base de données les réponses aux questions d'un candidat
     * ainsi que les catégories associées à chaque réponse
     *
     * @param array $id Liste des id des questions répondue
     * @param array $answer Liste des réponses à chaque question
     * @param array $category Liste des catégories attribuées à chaque réponse
     * @param string $titleForm Titre du formulaire
     *
     * @return void
     */
    public function insertListResponsesOfCandidate(array $id, array $answer, array $category, string $titleForm): void
    {
        $gatewayResponse = new GatewayResponse();
        $gatewayQuestion = new GatewayQuestion();

        $query = "INSERT INTO `listresponsesofcandidate`(date, titleForm) VALUES(:date, :titleForm)";
        $this->connection->executeQuery($query, array(
            ':date' => array(date('Y-m-d H:i:s'), PDO::PARAM_STR),
            ':titleForm' => array($titleForm, PDO::PARAM_STR)
        ));

        $idListQuestion = $this->connection->lastInsertId();

        for ($i = 0; $i < count($answer); $i++) {
            $question = $gatewayQuestion->getQuestionContentById($id[$i]);
            $idResponse = $gatewayResponse->insertResponse($question, $answer[$i], $category[$i]);

            $query = "INSERT INTO `submit` (responsesCandidate, response) VALUES(:responsesCandidate, :response)";
            $this->connection->executeQuery($query, array(
                ':responsesCandidate' => array($idListQuestion, PDO::PARAM_STR),
                ':response' => array($idResponse, PDO::PARAM_STR)
            ));
        }
    }

}
