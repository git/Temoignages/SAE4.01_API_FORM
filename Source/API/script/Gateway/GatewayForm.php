<?php

namespace Gateway;

use Config\ConnectClass;
use Config\Connection;
use PDO;
use PDOException;

class GatewayForm
{
    /**
     * @var Connection
     */
    private Connection $connection;

    public function __construct()
    {
        try{
            $this->connection = (new ConnectClass)->connect();
        }catch(PDOException $e){
            throw new PDOException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Permet d'instancier un formulaire s'il n'y a pas déjà un de présent dans la base de donnée
     * afin de rendre le formulaire modulable.
     *
     * @param string $title Titre du formulaire que l'on veut sauvegarder
     * @param string $description Description du formulaire que l'on veut sauvegarder
     *
     * @return void
     */
    public function insertForm(string $title, string $description): void
    {
        if(!empty($this->getForm()))
        {
            $query = "INSERT INTO `form`(title, description) VALUES(:title, :description)";
            $this->connection->executeQuery($query, array(
                ':title' => array($title, PDO::PARAM_STR),
                ':description' => array($description, PDO::PARAM_STR)
            ));
        }
    }

    /**
     * Permet de récupérer le formulaire sauvegarder dans la base de donnée.
     *
     * @return array Retourne un array contenant l'ensemble des données du formulaire
     */
    public function getForm(): array
    {
        $query = "SELECT * FROM `form`";
        $this->connection->executeQuery($query);
        return $this->connection->getResults();
    }

    /**
     * Permet de supprimer le formulaire dont l'id est le même que le formulaire passé en paramètre.
     *
     * @param int $idform Id du formulaire que l'on veut supprimer de la base de donnée
     *
     * @return void
     */
    public function deleteForm(int $idform): void
    {
        $query = "DELETE FROM `form` WHERE id = :id";
        $this->connection->executeQuery($query, array(
            ':id' => array($idform, PDO::PARAM_INT)
        ));
    }

    /**
     * Permet de faire le lien entre une catégorie et la réponse possible passées en paramètre
     * à la question dont l'id est aussi passé en paramètre.
     *
     * @param string $keyword Keyword que l'on veut associer
     * @param string $response Réponse que l'on veut associer
     * @param int $idQuestion Id de la question qui contient la réponse
     *
     * @return void
     */
    public function assignKeywordToQuestion(string $keyword, int $idQuestion, string $response): void
    {
        $query = "SELECT pr.id
                  FROM `propose` p, `possibleresponse` pr
                  WHERE p.question = :id AND p.possibleResponse = pr.id
                        AND pr.content = :response";
        $this->connection->executeQuery($query, array(
            ':id' => array($idQuestion, PDO::PARAM_INT),
            ':response' => array($response, PDO::PARAM_STR)
        ));

        $idPossibleResponse = $this->connection->getResults()[0][0];

        $query = "INSERT INTO `reference`(possibleResponse, keyword) VALUES(:possibleResponse, :keyword)";
        $this->connection->executeQuery($query, array(
            ':possibleResponse' => array($idPossibleResponse, PDO::PARAM_INT),
            ':keyword' => array($keyword, PDO::PARAM_STR)
        ));
    }

    /**
     * Permet de supprimer le lien entre une catégorie et la réponse possible passées en paramètre
     * à la question dont l'id est aussi passé en paramètre.
     *
     * @param string $keyword Keyword que l'on veut associer
     * @param int $idQuestion Id de la question qui contient la réponse
     * @param string $response Réponse que l'on veut associer
     *
     * @return void
     */
    public function deleteKeywordFromQuestion(string $keyword, int $idQuestion, string $response): void
    {
        $query = "SELECT pr.id FROM `propose` p, `possibleresponse` r
                  WHERE p.question = :id AND p.possibleResponse = pr.id
                        AND pr.content = :response";
        $this->connection->executeQuery($query, array(
            ':id' => array($idQuestion, PDO::PARAM_INT),
            ':response' => array($response, PDO::PARAM_STR)
        ));

        $idPossibleResponse = $this->connection->getResults()[0][0];

        $query = "DELETE FROM `reference` WHERE response = :idResponse AND keyword = :idKeyword";
        $this->connection->executeQuery($query, array(
            ':idResponse' => array($idPossibleResponse, PDO::PARAM_INT),
            ':idKeyword' => array($keyword, PDO::PARAM_INT)
        ));
    }

    /**
     * Permet de modifier le titre du formulaire sauvé dans la base de donnée.
     *
     * @param int $id Id du formulaire que l'on veut modifier
     * @param string $title Nouveau titre
     *
     * @return void
     */
    public function updateTitleToForm(int $id, string $title): void
    {
        $query = "UPDATE `form` SET title = :title WHERE id = :id";
        $this->connection->executeQuery($query, array(
            ':title' => array($title, PDO::PARAM_STR),
            ':id' => array($id, PDO::PARAM_INT)
        ));
    }

    /**
     * Permet de modifier la description du formulaire sauvé dans la base de donnée.
     *
     * @param int $id Id du formulaire que l'on veut modifier
     * @param string $description Nouvelle description
     *
     * @return void
     */
    public function updateDescriptionToForm(int $id, string $description): void
    {
        $query = "UPDATE `form` SET title = :title WHERE description = :description";
        $this->connection->executeQuery($query, array(
            ':description' => array($description, PDO::PARAM_STR),
            ':id' => array($id, PDO::PARAM_INT)
        ));
    }

    /**
     * Permet de supprimer la description du formulaire dans la base de donnée en le remplaçant par une chaine vide.
     *
     * @param int $idForm Id du formulaire que l'on veut modifier
     *
     * @return void
     */
    public function deleteDescriptionToForm(int $idForm): void
    {
        $query = "UPDATE `form` SET title = :title WHERE description = :description";
        $this->connection->executeQuery($query, array(
            ':description' => array('', PDO::PARAM_STR),
            ':id' => array($idForm, PDO::PARAM_INT)
        ));
    }

    /**
     * Permet de vérifier si un formulaire est présent dans la base de donnée
     *
     * @return bool
     */
    public function existsForm(): bool
    {
        $query = "SELECT * FROM `form`";
        $this->connection->executeQuery($query);
        return !empty($this->connection->getResults());
    }
}
