<?php

namespace Gateway;

use Config\Connection;
use Config\ConnectClass;
use PDO;
use PDOException;

class GatewayResponse
{
    private Connection $connection;

    public function __construct()
    {
        try{
            $this->connection = (new ConnectClass)->connect();
        }catch(PDOException $e){
            throw new PDOException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Permet de récupérer la liste des réponses d'un candidat ainsi que les Keyword couvert pour chaque réponse.
     *
     * @param int $listResponsesOfCandidateId Id du candidat
     *
     * @return array Retourne une liste contenant à l'indice 0 la liste de réponse
     * et à l'indice 1 une liste de Keyword,
     * où chaque indice d'une liste de question est associé au même indice une réponse
     */
    public function getCategorizeOfResponsesIdByIdListCandidate(int $listResponsesOfCandidateId): array
    {
        $result = $this->getResponsesByIdListCandidate($listResponsesOfCandidateId);
        $tab = [];
        foreach ($result as $row) {
            $tab[] = (new GatewayKeyword())->getKeywordsContentByCategorize($row['id']);
        }

        return array($result, $tab);
    }

    /**
     * Permet de récupérer la liste des réponses d'un candidat par son id.
     *
     * @param int $listResponsesOfCandidateId Id du candidat
     *
     * @return array Retourne la list des réponses du candidat
     */
    public function getResponsesByIdListCandidate(int $listResponsesOfCandidateId): array
    {

        $result = $this->getResponsesIdByIdListCandidate($listResponsesOfCandidateId);
        $tab = [];
        foreach ($result as $row){
            $tab[] = (new GatewayKeyword())->getKeywordsContentByReference($row['id']);
        }

        return array($result, $tab);
    }

    /**
     * Permet de récupérer la liste des id des réponses d'un candidat par son id.
     *
     * @param int $listResponsesOfCandidateId Id du candidat
     *
     * @return array Retourne la list des id des réponses du candidat
     */
    public function getResponsesIdByIdListCandidate(int $listResponsesOfCandidateId): array
    {
        $query = "SELECT r.id FROM `response` r, `submit` s WHERE s.responsesCandidate = :id AND r.id = s.response";
        $this->connection->executeQuery($query, array(
            ':id' => array($listResponsesOfCandidateId, PDO::PARAM_INT)
        ));
        return $this->connection->getResults();
    }

    /**
     * Permet de supprimer la réponse et ses liens avec les différents Keyword
     * selon l'id de la réponse passée en paramètre
     *
     * @param int $responseId Id de la réponse à supprimer
     *
     * @return void
     */
    public function deleteResponseById(int $responseId): void
    {
        $query = "DELETE FROM `categorize` WHERE response = :id";
        $this->connection->executeQuery($query, array(
            ':id' => array($responseId, PDO::PARAM_INT)
        ));

        $query = "DELETE FROM `submit` WHERE response = :id";
        $this->connection->executeQuery($query, array(
            ':id' => array($responseId, PDO::PARAM_INT)
        ));

        $query = "DELETE FROM `response` WHERE id = :id";
        $this->connection->executeQuery($query, array(
            ':id' => array($responseId, PDO::PARAM_INT)
        ));
    }

    /**
     * Permet d'insérer une réponse ainsi que de la lier avec les Keywords passés en paramètre.
     *
     * @param string $content Contenu de la réponse
     * @param string $questionContent Contenu de la question, dans le cas où la question viendra à changer dans le futur
     * @param array $category Liste des Keyword associé à la réponse
     *
     * @return int Retourne l'Id de la réponse insérée
     */
    public function insertResponse(string $content, string $questionContent, array $category): int
    {
        $query = "INSERT INTO `response`(content, questionContent) VALUES (:content, :questionContent)";
        $this->connection->executeQuery($query, array(
            ':content' => array($content, PDO::PARAM_STR),
            ':questionContent' => array($questionContent, PDO::PARAM_STR)
        ));

        $idResponse = $this->connection->lastInsertId();


        foreach ($category as $keyword){
            $query = "INSERT INTO `categorize` (response, keyword) VALUES(:response, :keyword)";
            $this->connection->executeQuery($query, array(
                ':response' => array($idResponse, PDO::PARAM_STR),
                ':keyword' => array($keyword, PDO::PARAM_STR)
            ));
        }

        return $idResponse;
    }

    /**
     * Permet de récupérer une liste de format de réponse définit par la commande sql passé en paramètre,
     * dont l'id du candidat, ayant formulé ces réponses, est passé en paramètre.
     *
     * @param string $query Requête du format de liste de réponse voulu
     * @param int $id Id du candidat cible
     *
     * @return array Retourne la liste des réponses d'un candidat selon le format voulu
     */
    public function getResponsesByQueryAndIdListCandidate(string $query, int $id): array
    {
        $this->connection->executeQuery($query, array(
            ':id' => array($id, PDO::PARAM_INT)
        ));
        return $this->connection->getResults();
    }

}