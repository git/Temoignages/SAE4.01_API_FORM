<?php

namespace Gateway;

use Config\Connection;
use Config\ConnectClass;
use PDO;
use PDOException;

class GatewayQuestion
{
    /**
     * @var Connection
     */
    private Connection $connection;

    public function __construct()
    {
        try{
            $this->connection = (new ConnectClass)->connect();
        }catch(PDOException $e){
            throw new PDOException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Permet d'ajouter une question dans la base de donnée.
     *
     * @param string $contentQuestion Information sur la question ajoutée : content
     * @param string $classQuestion Information sur la question ajoutée : type
     * @param int $idForm Id du formulaire associer
     *
     * @return int Id de la question ajoutée en base
     */
    public function addQuestion(string $contentQuestion, string $classQuestion, int $idForm): int
    {
        $query = "INSERT INTO `question`(content, type, form) VALUES(:content, :type, :form)";
        $this->connection->executeQuery($query, array(
            ':content' => array($contentQuestion, PDO::PARAM_STR),
            ':type' => array($classQuestion, PDO::PARAM_STR),
            ':form' => array($idForm, PDO::PARAM_INT)
        ));
        return $this->connection->lastInsertId();
    }

    /**
     * Ajoute une possible réponse à une question cible et associe à la possible réponse des catégories.
     *
     * @param string $response Contenu de la réponse possible
     * @param array $categories Liste des catégories attribuées à la possible réponse
     * @param int $idQuestion Id de la question associée à cette possible réponse
     *
     * @return void
     */
    public function insertResponseInQuestion(string $response, array $categories, int $idQuestion): void
    {
        $gatewayPossibleResponse = new GatewayPossibleResponse();
        $idPossibleResponse = $gatewayPossibleResponse->insertPossibleResponse($response);

        $query = "INSERT INTO `propose`(question, possibleResponse) VALUES(:question, :possibleResponse)";
        $this->connection->executeQuery($query, array(
            ':question' => array($idQuestion, PDO::PARAM_INT),
            ':possibleResponse' => array($idPossibleResponse, PDO::PARAM_INT)
        ));

        foreach ($categories as $keyword){
            $gatewayForm = new GatewayForm();
            $gatewayForm->assignKeywordToQuestion($keyword, $response, $idQuestion);
        }
    }

    /**
     * Permet de supprimer une question dans la base ainsi que ses dépendances
     *
     * @param string $questionClass
     * @param int $idQuestion Id de la question à supprimer
     * @return void
     */
    public function deleteQuestion(string $questionClass, int $idQuestion): void
    {
        if($questionClass == "BoxQuestionAPI") {
            $query = "DELETE FROM Propose WHERE question = :id";
            $this->connection->executeQuery($query, array(
                ':id' => array($idQuestion, PDO::PARAM_INT)
            ));
            $gatewayPossibleResponse = new GatewayPossibleResponse();
            $listPossibleResponse = $gatewayPossibleResponse->getPossibleResponseByQuestion($idQuestion);
            foreach ($listPossibleResponse as $row) {
                $gatewayPossibleResponse->deletePossibleResponse($row["id"]);
            }

            $query = "DELETE FROM Question WHERE id = :id";
            $this->connection->executeQuery($query, array(
                ':id' => array($idQuestion, PDO::PARAM_INT)
            ));

        }

        $query = "DELETE FROM `question` WHERE id = :id";
        $this->connection->executeQuery($query, array(
            ':id' => array($idQuestion, PDO::PARAM_INT)
        ));
    }

    /**
     * Permet de modifier dans la base de données les informations de la question.
     *
     * @param string $questionContent
     * @param string $questionClass
     * @param string $questionGetForm
     * @param string $idQuestion
     * @return void
     */
    public function updateQuestion(string $questionContent, string $questionClass, string $questionGetForm, string $idQuestion): void
    {
        $query = "UPDATE `question` SET  content = :content, type = :type, form = :form WHERE id = :id";
        $this->connection->executeQuery($query, array(
            ':content' => array($questionContent, PDO::PARAM_STR),
            ':type' => array($questionClass, PDO::PARAM_STR),
            ':form' => array($questionGetForm, PDO::PARAM_STR),
            ':id' => array($idQuestion, PDO::PARAM_STR)
        ));
    }

    /**
     * Permet de récupérer toutes les questions, possibles réponses et catégories associées d'un formulaire cible.
     *
     * @param int $idForm Id du formulaire
     *
     * @return array Retourne une liste qui pour
     * l'indice 0 la liste des questions
     * l'indice 1 une liste d'autres listes de réponses, qui pour chaque même indice correspond à la liste des réponses possibles de la question
     * l'indice 2 une liste d'autres listes de liste de catégories qui pour chaque indice de questions,
     * on a une liste d'autres listes de catégories qui pour chaque indice de réponses est associé une liste de catégories
     * Ou vide s'il n'y a pas de question dans la base
     */
    public function getAllQuestions(int $idForm): array
    {
        $query = "SELECT * FROM `question` WHERE form = :form";
        $this->connection->executeQuery($query, array(
            ':form' => array($idForm, PDO::PARAM_INT)
        ));

        $listQuestions = $this->connection->getResults();
        $possibleResponsesContent = [];
        $keywordsResponses = [];
        $gatewayKeyword = new GatewayKeyword();
        $gatewayPossibleResponse = new GatewayPossibleResponse();

        if(!empty($listQuestions)) {

            for ($i = 0; $i < count($listQuestions); $i++) {

                if ($listQuestions[$i]["type"] != "BusinessClass/TextQuestion") {
                    $possibleResponses = $gatewayPossibleResponse->getPossibleResponseByQuestion($listQuestions[$i]["id"]); //$this->connection->getResults();

                    $tmpTabKeyword = [];
                    $tmpTabPossibleResponse = [];
                    foreach ($possibleResponses as $row){
                        $tmpTabKeyword[] = $gatewayKeyword->getKeywordsContentByReference($row["id"]);
                        $tmpTabPossibleResponse[] = $row["content"];
                    }
                    $possibleResponsesContent[] = $tmpTabPossibleResponse;
                    $keywordsResponses[] = $tmpTabKeyword;
                }
                else{
                    $possibleResponsesContent[] = null;
                    $keywordsResponses[] = null;
                }
            }
            return array($listQuestions, $possibleResponsesContent, $keywordsResponses);
        }
        return array();
    }

    /**
     * Permet de récupérer le contenu d'une réponse à une question par son id.
     *
     * @param int $id Id de la question cible
     *
     * @return string Retourne le contenu de la question ciblé
     */
    public function getQuestionContentById(int $id): string
    {
        $query = "SELECT content FROM `question` WHERE id = :id";
        $this->connection->executeQuery($query, array(
            ':id' => array($id, PDO::PARAM_INT)
        ));

        return $this->connection->getResults()[0][0];
    }
}